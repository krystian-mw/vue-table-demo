module.exports = {
    useTabs: true,
    trailingComma: "none",
    vueIndentScriptAndStyle: true
};