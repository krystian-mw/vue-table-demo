red="\e[0;91m"
blue="\e[0;94m"
white_bg="\e[0;103m"
green="\e[0;92m"
white="\e[0;97m"
cyan="\e[0;36m"
yellow="\e[0;33m"
bold="\e[1m"
uline="\e[4m"
reset="\e[0m"

echo -e "${reset}${yellow}"

confirm() {
    read -r -n 1 -p "${1:-Have you inspected this script before running? [y/N]}" response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            echo -e "${green}"
            echo "Installing vue-table-demo ..."
            echo -e "${reset}"            
            # Setup project
            rm -rf vue-table-demo # remove project in case it exists
            git clone https://gitlab.com/krystian-mw/vue-table-demo # clone the project
            cd vue-table-demo # enter project dir
            yarn || npm i # install dependencies - ideally with yarn
            # Setup postgres
            sudo apt update # update dependencies
            sudo apt install -y postgresql # install (or try to isntall) pg            
            sudo -u postgres psql -c "create role root with login superuser password ''" # create user root with password root
            sudo -u postgres psql -c "create database root" # create user's db
            sudo -u postgres psql -c "create database sample" # create database 'sample' for the project
            sudo -u postgres psql -c "grant all privileges on database root to root" # grant privs to user db
            sudo -u postgres psql -c "grant all privileges on database sample to root"  # grant privs to project db
            # Setup database
            npx sequelize db:migrate # migrate db
            npx sequelize db:seed:all # enter sample data
            echo -e "${green}"
            printf "\n\nAll Set! Now run:\n\ncd vue-table-demo\nyarn dev\n\n\n"
            echo -e "${reset}"
            ;;
        *)
            echo -e "${red}"
            echo "Aborted"
            echo -e "${reset}"
            ;;
    esac
}
confirm

# Colours: https://techstop.github.io/bash-script-colors/
