[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/krystian-mw/vue-table-demo)

# Setup

## Gitpod

All set

## Linux

### One Line

All commands below put into a bash script

`curl -s https://gitlab.com/krystian-mw/vue-table-demo/-/raw/master/setup.sh > setup.sh && bash setup.sh`

### One or Couple of Steps

Copy and Paste all or pick/adjust what you need

```bash
# Setup project
git clone git@gitlab.com:krystian-mw/vue-table-demo.git # clone the project
cd vue-table-demo # enter project dir
yarn || npm i # install dependencies - ideally with yarn
# Setup postgres
sudo apt update # update dependencies
sudo apt install -y postgresql # install (or try to isntall) pg
sudo -u postgres psql -c "create role root with login superuser password ''" # create user root with password root
sudo -u postgres psql -c "create database root" # create user's db
sudo -u postgres psql -c "create database sample" # create database 'sample' for the project
sudo -u postgres psql -c "grant all privileges on database root to root" # grant privs to user db
sudo -u postgres psql -c "grant all privileges on database sample to root"  # grant privs to project db
# Setup database
npx sequelize db:migrate # migrate db
npx sequelize db:seed:all # enter sample data
```

## Windows

You should probably rethink your life choices.
Running this on Windows is as controversial as using `curl ... | bash`, so I will still provide instructions:

- Download and setup Postgres
- Configure `./config/config.json` accordingly to your postgres installation
- Run `npm i && npx sequelize db:migrate && npx sequelize db:seed:all`

# Usage

```bash
yarn dev # Start client and server in dev env
yarn lint # If eslint starts bugging you (try to use this as last resort)
yarn build # Compile client into production
```
