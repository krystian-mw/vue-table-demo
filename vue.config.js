module.exports = {
	devServer: {
		disableHostCheck: true,
		public: process.env.GITPOD_WORKSPACE_URL
			? process.env.GITPOD_WORKSPACE_URL.replace("https://", "https://8080-")
			: null
	}
};
